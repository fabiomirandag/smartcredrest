package br.com.newidea.smartcredrest.api;

import java.net.URI;

import org.springframework.http.RequestEntity;
import org.springframework.web.client.RestTemplate;

import br.com.newidea.smartcredrest.to.DashIndicadorTO;


public class ApiIndicador {
	
	public DashIndicadorTO getIndicador(String _uri, String cnpj) {
		RestTemplate restTemplate = new RestTemplate();
		char lastChar = _uri.charAt(_uri.length()-1);
		if ( "/".charAt(0) == lastChar ) 
			_uri = _uri.substring(0, _uri.length()-1); 

		URI uri = URI.create(_uri + "?cnpj=" + cnpj);

		RequestEntity<Void> request = RequestEntity.get(uri).build();
		return restTemplate.exchange(request, DashIndicadorTO.class).getBody();
	}
}
