package br.com.newidea.smartcredrest.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.newidea.smartcredrest.to.DashIndicadorTO;
import br.com.newidea.smartcredrest.to.LinhaDashIndicadorTO;
import br.com.newidea.smartcredrest.util.ConnectionFactory;


public class IndicadorDashPagtoEmDiaRepository {
	private String cnpj;
	
	public IndicadorDashPagtoEmDiaRepository(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public List<LinhaDashIndicadorTO> getLog() {
		List<LinhaDashIndicadorTO> list = new ArrayList<LinhaDashIndicadorTO>();
		
		String sqlStr = " select xy.datavencimento, " + 
						"        xy.datapagamento, " +
       					"        xy.valor from ( select a.datavencimento, " + 
						"                               a.datapagamento, " +
       					"                               a.valor from movimento a " +
						"                        inner join empresa b on (a.idEmpresa = b.id) " +
       					"                        where b.cnpj = ? ) as xy " +
						" limit 12 ";
		try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlStr);) {

			stm.setString(1, cnpj);
			try (ResultSet rs = stm.executeQuery();) {
				
				while (rs.next()) {
					NumberFormat formatter = new DecimalFormat("#0.00");     
					String valor = formatter.format(rs.getDouble(3));
					
					LinhaDashIndicadorTO linha = new LinhaDashIndicadorTO();
					linha.setLinha("Vencto: " + rs.getString(1) + "\n" + 
								   "Pagto: " + rs.getString(2) + "\n" +
								   "Valor: " + valor );
					list.add(linha);
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}				
	
		} catch (SQLException e1) {
			System.out.print(e1.getStackTrace());
		}
		
		
		return list;
	}
	
	public DashIndicadorTO getIndicador() {
		DashIndicadorTO dashIndicadorTO = new DashIndicadorTO();
		dashIndicadorTO.setDescricaoIndicador("Pagto Em Dia");
		
		String sqlStr = "select tot.nome_empresa, " + 
						" ((tot.qtde_tot_em_dia/tot.qtde_total) * 100) as percentual " + 
						" from (select a.nome as nome_empresa, " + 
						"   		   (select count(0) from movimento x " +
						"				where x.idEmpresa = a.id " +
						"				  and x.datapagamento is not null " +
						"                 and x.datapagamento <= x.datavencimento) as qtde_tot_em_dia, " +
						"              (select count(0) from movimento w " +
						"               where w.idEmpresa = a.id) as qtde_total " +
						"        from empresa a " +
						" where a.cnpj = ?) tot";
		
		
		try (Connection conn = ConnectionFactory.obtemConexao();
			 PreparedStatement stm = conn.prepareStatement(sqlStr);) {

			stm.setString(1, cnpj);
			try (ResultSet rs = stm.executeQuery();) {
				
				if (rs.next()) {
					NumberFormat formatter = new DecimalFormat("#0.00");     
					String percentual = formatter.format(rs.getDouble(2));
					
					dashIndicadorTO.setNomeEmpresa(rs.getString(1));
					dashIndicadorTO.setPercentualIndicador(percentual);
					dashIndicadorTO.setLog(getLog());
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}				
			
		} catch (SQLException e1) {
			System.out.print(e1.getStackTrace());
		}
		
		return dashIndicadorTO;
	}
}
