package br.com.newidea.smartcredrest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.newidea.smartcredrest.domain.Indicador;

public interface IndicadorRepository extends JpaRepository<Indicador, Long> {
	@Query("select i from Indicador i where i.ativo = ?#{[0]} ")
	List<Indicador> buscarAtivo(boolean ativo);
}
