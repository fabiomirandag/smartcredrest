package br.com.newidea.smartcredrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.newidea.smartcredrest.domain.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

}
