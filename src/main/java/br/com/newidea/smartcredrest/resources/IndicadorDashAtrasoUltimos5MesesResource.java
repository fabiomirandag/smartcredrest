package br.com.newidea.smartcredrest.resources;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.newidea.smartcredrest.repository.IndicadorDashAtrasoUltimos5MesesRepository;
import br.com.newidea.smartcredrest.to.DashIndicadorTO;

@RestController
@RequestMapping(value = "/indicadorDashAtrasoUltimos5Meses")
@CrossOrigin("*")
public class IndicadorDashAtrasoUltimos5MesesResource {

	@RequestMapping(method = RequestMethod.GET)
	public DashIndicadorTO getIndicador(@RequestParam("cnpj") String cnpj) {
		IndicadorDashAtrasoUltimos5MesesRepository indicador = new IndicadorDashAtrasoUltimos5MesesRepository(cnpj);
		return indicador.getIndicador();
	}
}
