package br.com.newidea.smartcredrest.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.newidea.smartcredrest.service.DashIndicadorService;
import br.com.newidea.smartcredrest.to.DashIndicadorTO;

@RestController
@RequestMapping(value = "/dashIndicador" )
@CrossOrigin("*")
public class DashboardResource {
	
	@Autowired
	private DashIndicadorService dashIndicadorService;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public List<DashIndicadorTO> listar(@RequestParam("cnpj") String cnpj) {
		return dashIndicadorService.listar(cnpj);
	}
}
