package br.com.newidea.smartcredrest.resources;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.newidea.smartcredrest.repository.IndicadorDashAtrasoUltimos6MesesRepository;
import br.com.newidea.smartcredrest.to.DashIndicadorTO;

@RestController
@RequestMapping(value = "/indicadorDashAtrasoUltimos6Meses")
@CrossOrigin("*")
public class IndicadorDashAtrasoUltimos6MesesResource {

	@RequestMapping(method = RequestMethod.GET)
	public DashIndicadorTO getIndicador(@RequestParam("cnpj") String cnpj) {
		IndicadorDashAtrasoUltimos6MesesRepository indicador = new IndicadorDashAtrasoUltimos6MesesRepository(cnpj);
		return indicador.getIndicador();
	}
}
