package br.com.newidea.smartcredrest.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.newidea.smartcredrest.domain.Indicador;
import br.com.newidea.smartcredrest.service.IndicadorService;



@RestController
@RequestMapping(value = "/indicador" )
@CrossOrigin("*")
public class IndicadorResource {
	@Autowired
	private IndicadorService indicadorService;
	
	@RequestMapping(method = RequestMethod.POST)
	public Indicador salvar(@RequestBody Indicador indicador) {
		return indicadorService.salvar(indicador);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Indicador> listar() {
		return indicadorService.listar();
	}
	

	@RequestMapping(method = RequestMethod.DELETE)
	public void deletar(@RequestBody Indicador indicador) {
		indicadorService.deletar(indicador);
	}
}
