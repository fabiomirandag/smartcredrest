package br.com.newidea.smartcredrest.resources;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.newidea.smartcredrest.repository.IndicadorDashAtrasoUltimos4MesesRepository;
import br.com.newidea.smartcredrest.to.DashIndicadorTO;


@RestController
@RequestMapping(value = "/indicadorDashAtrasoUltimos4Meses")
@CrossOrigin("*")
public class IndicadorDashAtrasoUltimos4MesesResource {
	@RequestMapping(method = RequestMethod.GET)
	public DashIndicadorTO getIndicador(@RequestParam("cnpj") String cnpj) {
		IndicadorDashAtrasoUltimos4MesesRepository indicador = new IndicadorDashAtrasoUltimos4MesesRepository(cnpj);
		return indicador.getIndicador();
	}
}
