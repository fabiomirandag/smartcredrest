package br.com.newidea.smartcredrest.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.newidea.smartcredrest.domain.Indicador;
import br.com.newidea.smartcredrest.repository.IndicadorRepository;

@Service
public class IndicadorService {
	
	@Autowired
	public IndicadorRepository indicadorRepository;
	
	public Indicador salvar(Indicador indicador) {
		return indicadorRepository.save(indicador);
	}
	
	public List<Indicador> listar() {
		return indicadorRepository.findAll();
	}
	
	public List<Indicador> buscarAtivo(boolean ativo) {
		return indicadorRepository.buscarAtivo(ativo);
	}
	
	public void deletar(Indicador indicador) {
		indicadorRepository.delete(indicador);
	}
}
