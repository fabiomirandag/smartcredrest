package br.com.newidea.smartcredrest.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.newidea.smartcredrest.api.ApiIndicador;
import br.com.newidea.smartcredrest.domain.Indicador;
import br.com.newidea.smartcredrest.to.DashIndicadorTO;

@Service
public class DashIndicadorService {
	
	@Autowired
	IndicadorService indicadorService;
	
	public List<DashIndicadorTO> listar(String cnpj) {
		List<DashIndicadorTO> dashIndicadores = new ArrayList<DashIndicadorTO> ();
		
		
		List<Indicador> indicadores = indicadorService.buscarAtivo(true);
		for (Indicador indicador : indicadores) {
			DashIndicadorTO to = new ApiIndicador().getIndicador(indicador.getUri(), cnpj);
			dashIndicadores.add(to);
		}
		
		return dashIndicadores;
	}
}
