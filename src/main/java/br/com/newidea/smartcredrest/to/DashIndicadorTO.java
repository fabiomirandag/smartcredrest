package br.com.newidea.smartcredrest.to;

import java.util.List;

public class DashIndicadorTO {

	private String nomeEmpresa;
	private String descricaoIndicador;
	private String percentualIndicador;
	private List<LinhaDashIndicadorTO> log;

	public DashIndicadorTO() {
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getDescricaoIndicador() {
		return descricaoIndicador;
	}

	public void setDescricaoIndicador(String descricaoIndicador) {
		this.descricaoIndicador = descricaoIndicador;
	}

	public String getPercentualIndicador() {
		return percentualIndicador;
	}

	public void setPercentualIndicador(String percentualIndicador) {
		this.percentualIndicador = percentualIndicador;
	}

	public List<LinhaDashIndicadorTO> getLog() {
		return log;
	}

	public void setLog(List<LinhaDashIndicadorTO> log) {
		this.log = log;
	}
}
