package br.com.newidea.smartcredrest.to;

public class LinhaDashIndicadorTO {
	private String linha;

	public LinhaDashIndicadorTO() {
	}

	public String getLinha() {
		return linha;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

}
