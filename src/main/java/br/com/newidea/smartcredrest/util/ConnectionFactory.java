package br.com.newidea.smartcredrest.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static Connection obtemConexao() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://smartcred.ci8hxtkwkcxs.sa-east-1.rds.amazonaws.com:3306/smartcred?user=newidea&password=12345678");
	}
}
